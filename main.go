package main

import (
	"CartApp/dao/mysql"
	"CartApp/routes"
	"fmt"
)

func main() {
	mysql.Init()
	// 注册路由
	r := routes.Setup(mysql.ConfigMode)
	err := r.Run(mysql.HttpPort)
	if err != nil {
		fmt.Printf("run server failed, err:%v\n", err)
		return
	}

}
