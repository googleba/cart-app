# CartApp商城系统

#### 介绍
CartApp在线商城，用户注册登录JWT 的验证的一个后台管理系统，支持日志切割管理，支持配制文件管理

支持swagger 文档管理 ，目前里面只是做了一个事例

用户注册登录

支持管理的信息的添加编辑，

支持用户的收货地址的增删改查，

支持订单的处理，

支持购物车操作

后续功能还要完善

如果觉得这个项目不错，您可以右上角 Star 支持一下！谢谢您的支持，您的支持是我完善的动力！

#### 软件架构
软件架构说明：go gin+gorm+mysql+redis+jwt-go


#### 安装教程

1. 本项目使用[Go Mod](https://github.com/golang/go/wiki/Modules)管理依赖。

   ```
   git clone git@gitee.com:googleba/cart-app.git
   cd cart-app
   go mod tidy
   go run main.go
   ```

   项目运行后启动在 7000 端口

## 目录结构

```
CartApp/
├── common
├── conf
├── controller
├── dao
├── logs
├── pkg
├── middleware
├── models
└── serializer
└── service
└── routes

```

- 用于定义接口函数

  ```
  ├── common :公共服务状态函数
  ├── conf :配制文件
  ├── controller ：接口层
  ├── dao：数据库逻辑层+路由逻辑处理
  ├── logs：日志层
  ├── pkg：jwt+核心函数+工具函数
  ├── middleware ：中间件
  ├── models ：模型层
  └── serializer：将数据序列化为 json 的函数
  └── service ：接口函数的实现
  └── routes
  ```
  
  


#### config.ini 文件配制详情

```
# 主页debug开发模式,release生产模式
[model]
AppDev = dev

# debug开发模式,release生产模式
[service]
AppMode = debug
HttpPort = :7000

[redis]
RedisDb = redis
RedisAddr = 127.0.0.1:6379
# redis ip地址和端口号
RedisPw = ""
# redis 密码
RedisDbName = 2
# redis 名字

[mysql]
Db = mysql
DbHost = 127.0.0.1
# mysql ip地址
DbPort = 3306
# mysql 端口号
DbUser = root
# mysql 用户名
DbPassWord = root
# mysql 密码
DbName = cart_app
# mysql 名字
```

#### 参与贡献
