package models

import "github.com/jinzhu/gorm"

type CreateAddressService struct {
	UserID  uint   `form:"user_id" json:"user_id"`
	Name    string `form:"name" json:"name"`
	Phone   string `form:"phone" json:"phone"`
	Address string `form:"address" json:"address"`
}

// Address 收货地址模型
type Address struct {
	gorm.Model
	UserID  uint
	Name    string
	Phone   string
	Address string
}

// UpdateAddressService 收货地址修改的服务
type UpdateAddressService struct {
	ID      uint   `form:"id" json:"id"`
	UserID  uint   `form:"user_id" json:"user_id"`
	Name    string `form:"name" json:"name"`
	Phone   string `form:"phone" json:"phone"`
	Address string `form:"address" json:"address"`
}

// DeleteAddressService 删除地址
type DeleteAddressService struct {
	AddressID uint `form:"address_id" json:"address_id"`
}
