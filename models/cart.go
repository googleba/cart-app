package models

import "github.com/jinzhu/gorm"

// CreateCartService 购物车创建的服务
type CreateCartService struct {
	UserID    uint `form:"user_id" json:"user_id"`
	ProductID uint `form:"product_id" json:"product_id"`
}

// Cart 订单模型
type Cart struct {
	gorm.Model
	UserID    uint
	ProductID uint
	Num       uint
	MaxNum    uint
	Check     bool
}

// UpdateCartService 购物车修改的服务
type UpdateCartService struct {
	UserID    uint `form:"user_id" json:"user_id"`
	ProductID uint `form:"product_id" json:"product_id"`
	Num       uint `form:"num" json:"num"`
}

// DeleteCartService 购物车删除的服务
type DeleteCartService struct {
	UserID    uint `form:"user_id" json:"user_id"`
	ProductID uint `form:"product_id" json:"product_id"`
}
