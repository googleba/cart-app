package models

import "github.com/jinzhu/gorm"

// CreateOrderService 订单创建的服务
type CreateOrderService struct {
	UserID    uint `form:"user_id" json:"user_id"`
	ProductID uint `form:"product_id" json:"product_id"`
	Num       uint `form:"num" json:"num"`
	AddressID uint `form:"address_id" json:"address_id"`
}

// Order 订单模型
type Order struct {
	gorm.Model
	UserID       uint
	ProductID    uint
	Num          uint
	OrderNum     uint64
	AddressName  string
	AddressPhone string
	Address      string
	Type         uint
}

// ListOrdersService 订单详情的服务
type ListOrdersService struct {
	Limit int  `form:"limit" json:"limit"`
	Start int  `form:"start" json:"start"`
	Type  uint `form:"type" json:"type" `
}
