package models

import (
	"github.com/jinzhu/gorm"
)

//UserRegisterService 用户注册服务
type UserService struct {
	UserName   string `form:"user_name" json:"user_name" binding:"required,min=3,max=15" example:"FanOne"`
	Password   string `form:"password" json:"password" binding:"required,min=5,max=16" example:"FanOne666"`
	RePassword string `form:"re_password" json:"re_password" binding:"required,min=5,max=16,eqfield=Password" example:"FanOne666"`
}

// 用户登录模型
type UserLogin struct {
	UserName string `form:"user_name" json:"user_name" binding:"required,min=3,max=15" example:"FanOne"`
	Password string `form:"password" json:"password" binding:"required,min=5,max=16" example:"FanOne666"`
}

//User 用户查询/创建模型
type User struct {
	gorm.Model
	UserName       string `gorm:"unique"`
	PasswordDigest string
	Post           []Post
}

// 修改用户信息
type UserEdit struct {
	ID       uint   `form:"id" json:"id" binding:"required"`
	UserName string `form:"user_name" json:"user_name" binding:"required,min=3,max=15" example:"FanOne"`
	Password string `form:"password" json:"password" binding:"required,min=5,max=16" example:"FanOne666"`
}
