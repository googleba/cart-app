package serializer

import (
	"CartApp/dao/mysql"
	"CartApp/models"
)

type User struct {
	ID       uint   `json:"id" form:"id" example:"1"`                    // 用户ID
	UserName string `json:"user_name" form:"user_name" example:"FanOne"` // 用户名
	Status   string `json:"status" form:"status"`                        // 用户状态
	CreateAt int64  `json:"create_at" form:"create_at"`                  // 创建
}

//BuildUser 序列化用户
func BuildUser(user models.User) User {
	return User{
		ID:       user.ID,
		UserName: user.UserName,
		CreateAt: user.CreatedAt.Unix(),
	}
}

type Post struct {
	ID        uint   `json:"id" example:"1"`          // 任务ID
	UserName  string `json:"user_name" example:"jim"` // 用户名
	Title     string `json:"title" example:"吃饭"`      // 题目
	Content   string `json:"content" example:"睡觉"`    // 内容
	Status    int    `json:"status" example:"0"`      // 状态(0未完成，1已完成)
	CreatedAt int64  `json:"created_at"`
	StartTime int64  `json:"start_time"`
	EndTime   int64  `json:"end_time"`
}

func BuildPost(item models.Post) Post {
	return Post{
		ID:        item.ID,
		UserName:  item.User,
		Title:     item.Title,
		Content:   item.Content,
		Status:    item.Status,
		CreatedAt: item.CreatedAt.Unix(),
		StartTime: item.StartTime,
		EndTime:   item.EndTime,
	}
}

func BuildPosts(items []models.Post) (posts []Post) {
	user := new(models.User)
	for _, item := range items {
		mysql.DB.Select("id,user_name").Where("id=?", item.ID).First(&user)
		item.User = user.UserName
		task := BuildPost(item)
		posts = append(posts, task)
	}
	return posts
}

type ListPostService struct {
	Limit int `form:"limit" json:"limit"`
	Size  int `form:"size" json:"size"`
}
