package mysql

import (
	"CartApp/conf"
	"CartApp/dao/redis"
	"CartApp/pkg/util"
	"strings"

	"gopkg.in/ini.v1"
)

var (
	ConfigMode string
	AppMode    string
	HttpPort   string
	Db         string
	DbHost     string
	DbPort     string
	DbUser     string
	DbPassWord string
	DbName     string
)

func Init() {
	file, err := ini.Load("./config.ini")
	if err != nil {
		util.Logger().Info("配置文件读取错误，请检查文件路径:", err)
		panic(err)
	}
	if err := conf.LoadLocales("conf/locales/zh-cn.yaml"); err != nil {
		util.Logger().Info(err) //日志内容
		panic(err)
	}
	LoadServer(file)
	LoadMysqlData(file)
	path := strings.Join([]string{DbUser, ":", DbPassWord, "@tcp(", DbHost, ":", DbPort, ")/", DbName, "?charset=utf8&parseTime=true"}, "")
	Database(path)
	redis.Redis()

	//配制main 主页开启模式
	MainConfig(file)
}

// 配制主页开启模式
func MainConfig(file *ini.File) {
	ConfigMode = file.Section("model").Key("AppDev").String()
}

func LoadServer(file *ini.File) {
	AppMode = file.Section("service").Key("AppMode").String()
	HttpPort = file.Section("service").Key("HttpPort").String()
}

func LoadMysqlData(file *ini.File) {
	Db = file.Section("mysql").Key("Db").String()
	DbHost = file.Section("mysql").Key("DbHost").String()
	DbPort = file.Section("mysql").Key("DbPort").String()
	DbUser = file.Section("mysql").Key("DbUser").String()
	DbPassWord = file.Section("mysql").Key("DbPassWord").String()
	DbName = file.Section("mysql").Key("DbName").String()
}
