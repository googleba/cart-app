package service

import (
	"CartApp/common"
	"CartApp/dao/mysql"
	"CartApp/models"
	"CartApp/pkg/e"
	"CartApp/serializer"

	logging "github.com/sirupsen/logrus"
)

func UserInfoModify(u *models.UserEdit) *serializer.Response {
	code := e.SUCCESS
	var user models.User
	err := mysql.DB.First(&user, u.ID).Error
	if err != nil {
		logging.Info(err)
		code = e.ErrorDatabase
		return &serializer.Response{
			Status: code,
			Msg:    e.GetMsg(code),
		}
	}
	password, err := common.SetPassword(u.Password)
	if err != nil {
		logging.Info(err)
		code = e.ErrorDatabase
		return &serializer.Response{
			Status: code,
			Msg:    e.GetMsg(code),
		}
	}
	user.UserName = u.UserName
	user.PasswordDigest = password
	err = mysql.DB.Save(&user).Error
	if err != nil {
		logging.Info(err)
		code = e.ErrorDatabase
		return &serializer.Response{
			Status: code,
			Msg:    e.GetMsg(code),
		}
	}
	return &serializer.Response{
		Status: code,
		Msg:    e.GetMsg(code),
	}

}

// 用户信息查看

func UserCheck(parseInt int64) *serializer.Response {
	code := e.SUCCESS
	user := new(models.User)
	err := mysql.DB.Debug().First(&user, parseInt).Error
	if err != nil {
		logging.Info(err)
		code = e.ErrorDatabase
		return &serializer.Response{
			Status: code,
			Msg:    e.GetMsg(code),
		}
	}
	return &serializer.Response{
		Status: code,
		Msg:    e.GetMsg(code),
		Data:   serializer.BuildUser(*user),
	}

}
