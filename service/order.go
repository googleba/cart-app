package service

import (
	"CartApp/dao/mysql"
	cache "CartApp/dao/redis"
	"CartApp/models"
	"CartApp/pkg/e"
	"CartApp/serializer"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"

	"github.com/go-redis/redis"
	logging "github.com/sirupsen/logrus"
)

// Create 创建订单
func CreateOrder(service *models.CreateOrderService) serializer.Response {
	order := models.Order{
		UserID:    service.UserID,
		ProductID: service.ProductID,
		Num:       service.Num,
		Type:      1,
	}
	address := models.Address{}
	code := e.SUCCESS
	//查找对应的地址
	if err := mysql.DB.First(&address, service.AddressID).Error; err != nil {
		logging.Info(err)
		code = e.ErrorDatabase
		return serializer.Response{
			Status: code,
			Msg:    e.GetMsg(code),
			Error:  err.Error(),
		}
	}
	order.AddressName = address.Name
	order.AddressPhone = address.Phone
	order.Address = address.Address
	//生成随机订单号
	number := fmt.Sprintf("%09v", rand.New(rand.NewSource(time.Now().UnixNano())).Int31n(1000000000))
	productNum := strconv.Itoa(int(service.ProductID))
	userNum := strconv.Itoa(int(service.UserID))
	number = number + productNum + userNum
	orderNum, err := strconv.ParseUint(number, 10, 64)
	if err != nil {
		logging.Info(err)
		code = e.ERROR
		return serializer.Response{
			Status: code,
			Msg:    e.GetMsg(code),
			Error:  err.Error(),
		}
	}
	order.OrderNum = orderNum
	//存入数据库
	err = mysql.DB.Create(&order).Error
	if err != nil {
		logging.Info(err)
		code = e.ErrorDatabase
		return serializer.Response{
			Status: code,
			Msg:    e.GetMsg(code),
			Error:  err.Error(),
		}
	}
	//将订单号存入Redis,并设置过期时间
	data := redis.Z{Score: float64(time.Now().Unix()) + 15*time.Minute.Seconds(), Member: orderNum}
	cache.RedisClient.ZAdd(os.Getenv("REDIS_ZSET_KEY"), data)

	return serializer.Response{
		Status: code,
		Msg:    e.GetMsg(code),
	}
}

// List 订单
func List(id string, service *models.ListOrdersService) serializer.Response {
	var orders []models.Order
	total := 0
	code := e.SUCCESS
	if service.Limit == 0 {
		service.Limit = 5
	}

	if service.Type == 0 {
		if err := mysql.DB.Model(&orders).Where("user_id=?", id).Count(&total).Error; err != nil {
			logging.Info(err)
			code = e.ErrorDatabase
			return serializer.Response{
				Status: code,
				Msg:    e.GetMsg(code),
				Error:  err.Error(),
			}
		}

		if err := mysql.DB.Where("user_id=?", id).Limit(service.Limit).Offset(service.Start).Order("created_at desc").Find(&orders).Error; err != nil {
			logging.Info(err)
			code = e.ErrorDatabase
			return serializer.Response{
				Status: code,
				Msg:    e.GetMsg(code),
				Error:  err.Error(),
			}
		}
	} else {
		if err := mysql.DB.Model(&orders).Where("user_id=? AND type=?", id, service.Type).Count(&total).Error; err != nil {
			logging.Info(err)
			code = e.ErrorDatabase
			return serializer.Response{
				Status: code,
				Msg:    e.GetMsg(code),
				Error:  err.Error(),
			}
		}

		if err := mysql.DB.Where("user_id=? AND type=?", id, service.Type).Limit(service.Limit).Offset(service.Start).Order("created_at desc").Find(&orders).Error; err != nil {
			logging.Info(err)
			code = e.ErrorDatabase
			return serializer.Response{
				Status: code,
				Msg:    e.GetMsg(code),
				Error:  err.Error(),
			}
		}
	}
	return serializer.BuildListResponse(serializer.BuildOrders(orders), uint(total))
}

// Show 订单
func OrderDetail(num string) serializer.Response {
	var order models.Order
	var product models.Product
	code := e.SUCCESS
	//根据id查找order
	if err := mysql.DB.Where("order_num=?", num).First(&order).Error; err != nil {
		logging.Info(err)
		code = e.ErrorDatabase
		return serializer.Response{
			Status: code,
			Msg:    e.GetMsg(code),
		}
	}
	//根据order查找product
	if err := mysql.DB.Where("id=?", order.ProductID).First(&product).Error; err != nil {
		//如果查询不到，返回相应错误
		if gorm.IsRecordNotFoundError(err) {
			logging.Info(err)
			code = e.ErrorDatabase
			return serializer.Response{
				Status: code,
				Msg:    e.GetMsg(code),
			}
		}
		logging.Info(err)
		code = e.ErrorDatabase
		return serializer.Response{
			Status: code,
			Msg:    e.GetMsg(code),
		}
	}
	return serializer.Response{
		Status: code,
		Msg:    e.GetMsg(code),
		Data:   serializer.BuildOrder(order, product),
	}
}
