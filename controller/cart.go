package controller

import (
	"CartApp/models"
	"CartApp/service"

	"github.com/gin-gonic/gin"
	logging "github.com/sirupsen/logrus"
)

func CreateCart(c *gin.Context) {
	server := models.CreateCartService{}
	if err := c.ShouldBind(&server); err == nil {
		res := service.CreateCart(&server)
		c.JSON(200, res)
	} else {
		c.JSON(200, ErrorResponse(err))
		logging.Info(err)
	}
}

// ShowCarts 购物车详情接口
func ShowCarts(c *gin.Context) {
	res := service.ShowCarts(c.Param("id"))
	c.JSON(200, res)
}

// UpdateCart 修改购物车信息
func UpdateCart(c *gin.Context) {
	server := models.UpdateCartService{}
	if err := c.ShouldBind(&server); err == nil {
		res := service.UpdateCart(&server)
		c.JSON(200, res)
	} else {
		c.JSON(200, ErrorResponse(err))
		logging.Info(err)
	}
}

// DeleteCart 删除购物车
func DeleteCart(c *gin.Context) {
	server := models.DeleteCartService{}
	if err := c.ShouldBind(&server); err == nil {
		res := service.DeleteCart(&server)
		c.JSON(200, res)
	} else {
		c.JSON(200, ErrorResponse(err))
		logging.Info(err)
	}
}
