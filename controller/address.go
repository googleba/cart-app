package controller

import (
	"CartApp/models"
	"CartApp/service"

	"github.com/gin-gonic/gin"
	logging "github.com/sirupsen/logrus"
)

func CreateAddress(c *gin.Context) {
	server := models.CreateAddressService{}
	if err := c.ShouldBind(&server); err == nil {
		res := service.Create(&server)
		c.JSON(200, res)
	} else {
		c.JSON(200, ErrorResponse(err))
		logging.Info(err)
	}
}

func ShowAddresses(c *gin.Context) {
	res := service.Show(c.Param("id"))
	c.JSON(200, res)
}

// UpdateAddress 修改收货地址
func UpdateAddress(c *gin.Context) {
	server := models.UpdateAddressService{}
	if err := c.ShouldBind(&server); err == nil {
		res := service.Update(&server)
		c.JSON(200, res)
	} else {
		c.JSON(200, ErrorResponse(err))
		logging.Info(err)
	}
}

// DeleteAddress 删除收货地址
func DeleteAddress(c *gin.Context) {
	server := models.DeleteAddressService{}
	if err := c.ShouldBind(&server); err == nil {
		res := service.Delete(&server)
		c.JSON(200, res)
	} else {
		c.JSON(200, ErrorResponse(err))
		logging.Info(err)
	}
}
