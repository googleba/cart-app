package controller

import (
	"CartApp/models"
	"CartApp/service"

	"github.com/gin-gonic/gin"
	logging "github.com/sirupsen/logrus"
)

// CreateOrder 创建订单
func CreateOrder(c *gin.Context) {
	server := models.CreateOrderService{}
	if err := c.ShouldBind(&server); err == nil {
		res := service.CreateOrder(&server)
		c.JSON(200, res)
	} else {
		c.JSON(200, ErrorResponse(err))
		logging.Info(err)
	}
}

// ListOrders 订单详情接口
func ListOrders(c *gin.Context) {
	server := models.ListOrdersService{}
	if err := c.ShouldBind(&server); err == nil {
		res := service.List(c.Param("id"), &server)
		c.JSON(200, res)
	} else {
		c.JSON(200, ErrorResponse(err))
		logging.Info(err)
	}
}

// ShowOrder 订单详情详情接口
func ShowOrder(c *gin.Context) {
	res := service.OrderDetail(c.Param("num"))
	c.JSON(200, res)
}
