package controller

import (
	"CartApp/models"
	"CartApp/pkg/util"
	"CartApp/service"

	//"gopkg.in/go-playground/validator.v8"
	"github.com/gin-gonic/gin"
)

// 用户注册
func UserRegisterHandler(c *gin.Context) {
	userRegisterService := new(models.UserService)
	if err := c.ShouldBindJSON(userRegisterService); err != nil {
		util.Logger().Error("UserRegisterHandler failed", err)
		c.JSON(400, ErrorResponse(err))
		return
	}
	res := service.Register(userRegisterService)
	c.JSON(200, res)
	return
}

// 用户登录
func UserLoginHandler(c *gin.Context) {
	userLoginService := new(models.UserLogin)
	if err := c.ShouldBindJSON(&userLoginService); err == nil {
		res := service.Login(userLoginService)
		c.JSON(200, res)
	} else {
		c.JSON(400, ErrorResponse(err))
		util.Logger().Info(err)
	}
}
