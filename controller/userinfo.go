package controller

import (
	"CartApp/models"
	"CartApp/service"
	"strconv"

	"github.com/gin-gonic/gin"
)

func UserInfoModifyHandler(c *gin.Context) {
	u := new(models.UserEdit)
	if err := c.ShouldBind(u); err != nil {
		c.JSON(400, ErrorResponse(err))
		return
	}
	res := service.UserInfoModify(u)
	c.JSON(200, res)
	return

}

// 用户信息查看
func UserCheckHandler(c *gin.Context) {
	paramID := c.Param("id")
	parseInt, err := strconv.ParseInt(paramID, 10, 64)
	if err != nil {
		c.JSON(400, ErrorResponse(err))
		return
	}
	res := service.UserCheck(parseInt)
	c.JSON(200, res)

}
