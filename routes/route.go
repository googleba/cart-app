package routes

import (
	"CartApp/controller"
	"CartApp/middleware"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Setup(mode string) *gin.Engine {
	if mode == gin.ReleaseMode {
		gin.SetMode(gin.ReleaseMode) // 设置成发布模式
	}
	// 路由部份
	r := gin.New()
	r.Use(middleware.NewLogger(), middleware.Cors(), gin.Recovery())
	v1 := r.Group("api/v1")
	{
		// user opation
		v1.POST("/register", controller.UserRegisterHandler)
		v1.POST("/login", controller.UserLoginHandler)

		v1.Use(middleware.JWT())
		{
			v1.POST("post", controller.CreatePostHandler)
			v1.PUT("post", controller.UpdatePostHandler)
			v1.GET("post/:id", controller.GetPostHandler)
			v1.GET("postList", controller.PostListHandler)
			//user hasmany post
			v1.GET("user-post-list", controller.UserPostListHandler)
			//user info modify
			v1.GET("user-check/:id", controller.UserCheckHandler)
			v1.POST("useInfo", controller.UserInfoModifyHandler)
			//Order operation
			v1.POST("orders", controller.CreateOrder)
			v1.GET("user/:id/orders", controller.ListOrders)
			v1.GET("orders/:num", controller.ShowOrder)
			//cart operation
			v1.POST("carts", controller.CreateCart)
			v1.GET("carts/:id", controller.ShowCarts)
			v1.PUT("carts", controller.UpdateCart)
			v1.DELETE("carts", controller.DeleteCart)
			//adderss opation
			v1.POST("addresses", controller.CreateAddress)
			v1.GET("addresses/:id", controller.ShowAddresses)
			v1.PUT("addresses", controller.UpdateAddress)
			v1.DELETE("addresses", controller.DeleteAddress)
		}
	}

	r.NoRoute(func(c *gin.Context) {
		c.String(http.StatusNotFound, "暂无数据!")
	})
	return r

}
